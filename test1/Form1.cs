﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace test1
{
    public partial class Form1 : Form
    {
        System.Timers.Timer t;

        #region Gerekli Kodlar
        private const int MAXITEMCOUNT = 30;
        public struct listItem
        {
            public string Text;
            public Color Color;
            public override string ToString()
            {
                return Text;
            }
        }
        private void listBox1_DrawItem(object sender, DrawItemEventArgs e)
        {
            ListBox theListBox = (ListBox)sender;
            listBoxGeneric.ItemHeight= theListBox.Font.Height;
            if (e.Index < 0) return;
            int index = e.Index;
            Graphics g = e.Graphics;

            e.DrawBackground();
            g.FillRectangle(new SolidBrush(((listItem)listBoxGeneric.Items[e.Index]).Color), e.Bounds);

            // Get the item details
            Font font = listBoxGeneric.Font;
            Color colour = listBoxGeneric.ForeColor;
            string text = listBoxGeneric.Items[index].ToString();

            // Print the text
            g.DrawString(text, font , new SolidBrush(Color.Black), (float)e.Bounds.X, (float)e.Bounds.Y);
            e.DrawFocusRectangle();
        }
        public void InsertItem(String Text, int red, int green, int blue)
        {
            listItem ii;
            ii.Text = Text;
            ii.Color = Color.FromArgb(red, green, blue);
            if (listBoxGeneric.InvokeRequired)
            {
                listBoxGeneric.Invoke((MethodInvoker)(() =>
                {
                    listBoxGeneric.Items.Insert(0, ii);

                    if (listBoxGeneric.Items.Count > MAXITEMCOUNT)
                        listBoxGeneric.Items.RemoveAt(listBoxGeneric.Items.Count - 1);
                }));
            }
            else
            {
                listBoxGeneric.Items.Insert(0, ii);

                if (listBoxGeneric.Items.Count > MAXITEMCOUNT)
                    listBoxGeneric.Items.RemoveAt(listBoxGeneric.Items.Count - 1);
            }
        }
        public void RemoveItem()
        {
            if (listBoxGeneric.Items.Count > 0)
                listBoxGeneric.Items.RemoveAt(listBoxGeneric.Items.Count - 1);
        }
        #endregion

        #region Test Kodları
        int n = 0;
        Random rnd;
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            rnd = new Random();
            t = new System.Timers.Timer();
            t.Elapsed += T_Elapsed;
            t.Interval = 1000;
            t.Start();
        }

        private void T_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            InsertItem("Satır " + n++, rnd.Next(0, 255), rnd.Next(0, 255), rnd.Next(0, 255));
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            InsertItem("Satır " + n++, rnd.Next(0, 255), rnd.Next(0, 255), rnd.Next(0, 255));
        }
        private void btnRemove_Click(object sender, EventArgs e)
        {
            RemoveItem();
        }
        #endregion
    }
}
